# oil-sardine


A WebDAV client for Java applications, using [OkHttp](https://github.com/square/okhttp) as HTTP client.

The goal is to have a pure Java library that is as lean as possible, without dependencies on Kotlin Standard library, JAXB or Apache's HttpClient. 

You still need: 
- an okhttp version without Kotlin, e.g. [okhttp-3.14.0.jar](https://repo1.maven.org/maven2/com/squareup/okhttp3/okhttp/3.14.0/)
- a compatible okio library, e.g. [okio-1.17.5.jar](https://repo1.maven.org/maven2/com/squareup/okio/okio/1.17.5/)
- the simple xml library, e.g. [simple-xml-2.7.1.jar](http://simple.sourceforge.net/download.php)

If you are looking for a WebDAV client for Android, you better go to [Grizzly Labs sardine-android](https://github.com/thegrizzlylabs/sardine-android), if you are looking for a WebDAV client for Java applications and dependencies on large libraries is not an issue, you can go to [Sardine library](https://github.com/lookfirst/sardine). 



## Getting started


- Create a `Sardine` client:
```
Sardine sardine = new OkHttpSardine();
sardine.setCredentials("username", "password");
```

- Use the client to make requests to your WebDAV server:
```
List<DavResource> resources = sardine.list("http://webdav.server.com");
```

## Legacy

Originally forked from [Grizzly Labs' fork](https://github.com/thegrizzlylabs/sardine-android) of the [Sardine library](https://github.com/lookfirst/sardine). 
All Android dependencies and libraries that require the Kotlin standard library have been removed.

